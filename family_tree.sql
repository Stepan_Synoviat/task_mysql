-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema my_at_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema my_at_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `my_at_db` DEFAULT CHARACTER SET utf8 ;
USE `my_at_db` ;

-- -----------------------------------------------------
-- Table `my_at_db`.`sex`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `my_at_db`.`sex` (
  `idsex` INT NOT NULL AUTO_INCREMENT,
  `sex` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idsex`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `my_at_db`.`family_satellite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `my_at_db`.`family_satellite` (
  `idfamily_satellites` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NULL,
  `date_of_birth` DATE NOT NULL,
  `birthplace` VARCHAR(45) NOT NULL,
  `date_of_death` DATE NULL,
  `deathplace` VARCHAR(45) NULL,
  `date_of_marriage` VARCHAR(45) NOT NULL,
  `sex_idsex` INT NOT NULL,
  `full_name` VARCHAR(45) GENERATED ALWAYS AS (concat(`name`,_utf8mb3' ',`surname`)) VIRTUAL,
  PRIMARY KEY (`idfamily_satellites`),
  INDEX `fk_family_satellite_sex1_idx` (`sex_idsex` ASC) INVISIBLE,
  CONSTRAINT `fk_family_satellite_sex1`
    FOREIGN KEY (`sex_idsex`)
    REFERENCES `my_at_db`.`sex` (`idsex`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `my_at_db`.`family_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `my_at_db`.`family_tree` (
  `idfamily_tree` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `birthdate` DATE NOT NULL,
  `birthplace` VARCHAR(45) NOT NULL,
  `deathdate` DATE NULL,
  `deathplace` VARCHAR(45) NULL,
  `credit_card_number` VARCHAR(45) NOT NULL,
  `parent` INT NULL,
  `family_satellite_idfamily_satellites` INT NOT NULL,
  `sex_idsex` INT NOT NULL,
  `full_name` VARCHAR(45) GENERATED ALWAYS AS (concat(`name`,_utf8mb3' ',`surname`)) VIRTUAL,
  PRIMARY KEY (`idfamily_tree`),
  INDEX `fk_family_tree_family_satellite_idx` (`family_satellite_idfamily_satellites` ASC) INVISIBLE,
  INDEX `fk_family_tree_sex1_idx` (`sex_idsex` ASC) INVISIBLE,
  INDEX `parent_id_fk` (`parent` ASC) INVISIBLE,
  CONSTRAINT `fk_family_tree_family_satellite`
    FOREIGN KEY (`family_satellite_idfamily_satellites`)
    REFERENCES `my_at_db`.`family_satellite` (`idfamily_satellites`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_sex1`
    FOREIGN KEY (`sex_idsex`)
    REFERENCES `my_at_db`.`sex` (`idsex`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `parent_id_fk`
    FOREIGN KEY (`parent`)
    REFERENCES `my_at_db`.`family_tree` (`idfamily_tree`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `my_at_db`.`family_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `my_at_db`.`family_values` (
  `idfamily_values` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `indicative_cost` DECIMAL(12) NOT NULL,
  `max_cost` DECIMAL(12) NOT NULL,
  `min_cost` DECIMAL(12) NOT NULL,
  `cod_from_catalog` VARCHAR(45) NOT NULL,
  `cost_coefficient` INT GENERATED ALWAYS AS ((`min_cost` + `max_cost`)) VIRTUAL,
  PRIMARY KEY (`idfamily_values`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `my_at_db`.`family_tree_has_family_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `my_at_db`.`family_tree_has_family_values` (
  `id` INT NOT NULL,
  `family_tree_idfamily_tree` INT NOT NULL,
  `family_values_idfamily_values` INT NOT NULL,
  INDEX `fk_family_tree_has_family_values_family_values1_idx` (`family_values_idfamily_values` ASC) VISIBLE,
  INDEX `fk_family_tree_has_family_values_family_tree1_idx` (`family_tree_idfamily_tree` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_family_tree_has_family_values_family_tree1`
    FOREIGN KEY (`family_tree_idfamily_tree`)
    REFERENCES `my_at_db`.`family_tree` (`idfamily_tree`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_has_family_values_family_values1`
    FOREIGN KEY (`family_values_idfamily_values`)
    REFERENCES `my_at_db`.`family_values` (`idfamily_values`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
