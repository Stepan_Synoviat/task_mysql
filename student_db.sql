-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema student_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema student_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `student_db` DEFAULT CHARACTER SET utf8 ;
USE `student_db` ;

-- -----------------------------------------------------
-- Table `student_db`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_db`.`group` (
  `idgroup` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `group_number` VARCHAR(45) NOT NULL,
  `year_of_arrival` VARCHAR(45) NULL,
  PRIMARY KEY (`idgroup`),
  UNIQUE INDEX `group_number_UNIQUE` (`group_number` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_db`.`arrear`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_db`.`arrear` (
  `id_arrears` INT NOT NULL,
  `arrear` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_arrears`),
  UNIQUE INDEX `arrear_UNIQUE` (`arrear` ASC) VISIBLE,
  UNIQUE INDEX `id_arrears_UNIQUE` (`id_arrears` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_db`.`region`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_db`.`region` (
  `id_region` INT NOT NULL,
  `region` VARCHAR(45) NOT NULL,
  `region_code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_region`),
  UNIQUE INDEX `id_region_UNIQUE` (`id_region` ASC) VISIBLE,
  UNIQUE INDEX `region_UNIQUE` (`region` ASC) VISIBLE,
  UNIQUE INDEX `region_code_UNIQUE` (`region_code` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_db`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_db`.`city` (
  `idcity` INT NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `region_id_region` INT NOT NULL,
  PRIMARY KEY (`idcity`),
  UNIQUE INDEX `idcity_UNIQUE` (`idcity` ASC) VISIBLE,
  UNIQUE INDEX `city_UNIQUE` (`city` ASC) VISIBLE,
  INDEX `fk_city_region1_idx` (`region_id_region` ASC) VISIBLE,
  CONSTRAINT `fk_city_region1`
    FOREIGN KEY (`region_id_region`)
    REFERENCES `student_db`.`region` (`id_region`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_db`.`graduated_from_school`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_db`.`graduated_from_school` (
  `idgraduated_from_school` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `telephone_number` VARCHAR(45) NOT NULL,
  `direcrors_name` VARCHAR(45) NOT NULL,
  `city_idcity` INT NOT NULL,
  PRIMARY KEY (`idgraduated_from_school`),
  INDEX `fk_graduated_from_school_city1_idx` (`city_idcity` ASC) VISIBLE,
  CONSTRAINT `fk_graduated_from_school_city1`
    FOREIGN KEY (`city_idcity`)
    REFERENCES `student_db`.`city` (`idcity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_db`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_db`.`student` (
  `idstudents` INT NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `middle_name` VARCHAR(45) NOT NULL,
  `date_of_birth` VARCHAR(45) NOT NULL,
  `date_of_arrival` VARCHAR(45) NOT NULL,
  `student_card_number` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL,
  `full_name` VARCHAR(45) GENERATED ALWAYS AS (concat(surname ,' ', name ,' ', middle_name)) VIRTUAL,
  `arrear_id_arrears` INT NULL,
  `city_idcity` INT NOT NULL,
  `graduated_from_school_idgraduated_from_school` INT NOT NULL,
  `group_idgroup` INT NOT NULL,
  `diference_between_years` INT GENERATED ALWAYS AS (date_of_arrival-date_of_birth) VIRTUAL,
  PRIMARY KEY (`idstudents`),
  INDEX `fk_student_group1_idx` (`group_idgroup` ASC) VISIBLE,
  UNIQUE INDEX `idstudents_UNIQUE` (`idstudents` ASC) VISIBLE,
  INDEX `fk_student_arrear1_idx` (`arrear_id_arrears` ASC) VISIBLE,
  INDEX `fk_student_city1_idx` (`city_idcity` ASC) VISIBLE,
  INDEX `fk_student_graduated_from_school1_idx` (`graduated_from_school_idgraduated_from_school` ASC) INVISIBLE,
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_idgroup`)
    REFERENCES `student_db`.`group` (`idgroup`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_arrear1`
    FOREIGN KEY (`arrear_id_arrears`)
    REFERENCES `student_db`.`arrear` (`id_arrears`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_city1`
    FOREIGN KEY (`city_idcity`)
    REFERENCES `student_db`.`city` (`idcity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_graduated_from_school1`
    FOREIGN KEY (`graduated_from_school_idgraduated_from_school`)
    REFERENCES `student_db`.`graduated_from_school` (`idgraduated_from_school`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
